import React, { Fragment, FunctionComponent } from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import { Header } from '../components/Header';
import { TicketsView } from '../ticket-management/TicketsView';
import { TicketForm } from '../ticket-management/TicketForm';
import { NotFound } from '../components/NotFound';
import { SuccessSnackbar } from '../components/SuccessSnackbar';

export interface RouteProps {
  key: string;
  path: string;
  component: FunctionComponent;
  showAddButton?: boolean;
}

export enum RouteLocations {
  TICKET_FORM = '/ticket-info',
  TICKETS = '/tickets'
}

export const routes: RouteProps[] = [
  {
    key: 'TICKETS',
    path: RouteLocations.TICKETS,
    component: TicketsView,
    showAddButton: true
  },
  {
    key: 'TICKET_INFO',
    path: RouteLocations.TICKET_FORM,
    component: TicketForm
  }
];

export const RenderRoute = (route: RouteProps) => (
  <>
    <Header showAddButton={route.showAddButton} />
    <Route path={route.path} component={route.component} />
    <SuccessSnackbar />
  </>
);

export const RenderRoutes = () => (
  <Switch>
    <Route exact path="/">
      <Redirect to={RouteLocations.TICKETS} />
    </Route>
    {routes.map((route: RouteProps) => <RenderRoute {...route} key={route.key} />)}
    <Route component={NotFound} />
  </Switch>
);
