import { atom, AtomEffect, DefaultValue } from 'recoil';

const localStorageEffect: <T>(key: string) => AtomEffect<T> = (
  key: string
) => ({ setSelf, onSet }) => {
  const savedValue = localStorage.getItem(key);
  if (savedValue) {
    setSelf(JSON.parse(savedValue));
  }

  onSet((newValue: any) => {
    if (newValue instanceof DefaultValue) {
      localStorage.removeItem(key);
    } else {
      localStorage.setItem(key, JSON.stringify(newValue));
    }
  });
};

export interface Ticket {
  key: string;
  description: string;
  creationDate: Date;
  dueDate: Date;
}

export const ticketDataState = atom({
  key: 'ticketDataState',
  default: [] as Ticket[],
  effects_UNSTABLE: [
    localStorageEffect('ticket_data')
  ]
});
