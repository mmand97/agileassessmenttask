import { atom } from 'recoil';

export const snackbarOpenState = atom({
  key: 'snackbarOpenState',
  default: false
});
