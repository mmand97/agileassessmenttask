import { format, parseISO } from 'date-fns';

export const DATE_TIME_FORMAT = 'dd.MM.yyyy HH:mm';

export const dateToString = (date: Date | null) => {
  if (!date) {
    return null;
  }
  return format(new Date(date), DATE_TIME_FORMAT);
};

export const getTime = (date: Date | null) => {
  if (!date) {
    return 0;
  }
  return new Date(date).getTime();
};
