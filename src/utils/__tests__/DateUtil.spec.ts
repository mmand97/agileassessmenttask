import { dateToString, getTime } from '../DateUtil';

describe('DateUtil', () => {
  describe('dateToString', () => {
    it('should return date string when date is not null', () => {
      expect(dateToString(new Date('11.11.2022 22:10'))).toBe('11.11.2022 22:10');
    });

    it('should return null when date is null', () => {
      expect(dateToString(null)).toBe(null);
    });
  });

  describe('getTime', () => {
    it('should return time when date is not null', () => {
      expect(getTime(new Date(Date.UTC(2022, 11, 11, 12, 15)))).toBe(1670760900000);
    });

    it('should return null when date is null', () => {
      expect(getTime(null)).toBe(0);
    });
  });
});
