import { useRecoilValue } from 'recoil';
import { useEffect } from 'react';

// @ts-ignore
export const RecoilObserver = ({ node, onSubmit }) => {
  const value = useRecoilValue(node);
  useEffect(() => onSubmit(value), [onSubmit, value]);
  return null;
};
