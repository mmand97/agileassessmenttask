import { Ticket } from '../store/TicketStore';

export const overDueTicketsMockData: Ticket[] = [
  {
    key: '00000000-0000-0000-0000-000000000000',
    description: 'test description',
    creationDate: new Date('01-01-2020'),
    dueDate: new Date('01-01-2020')
  }
];

export const ticketsMockData: Ticket[] = [
  {
    key: '00000000-0000-0000-0000-000000000000',
    description: 'test description',
    creationDate: new Date('01-01-2022'),
    dueDate: new Date('01-01-2022')
  }
];
