import React from 'react';
import { Alert, Snackbar } from '@mui/material';
import { useRecoilState } from 'recoil';
import { snackbarOpenState } from '../store/SnackbarStore';

export const SuccessSnackbar = () => {
  const [snackbarOpen, setSnackbarOpen] = useRecoilState<boolean>(snackbarOpenState);

  return (
    <div>
      <Snackbar
        role="alert"
        autoHideDuration={6000}
        open={snackbarOpen}
        onClose={() => setSnackbarOpen(false)}
        anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
      >
        <Alert
          id="snackbarSuccessMessage"
          onClose={() => setSnackbarOpen(false)}
          severity="success"
          sx={{ width: '100%' }}
        >
          Action success!
        </Alert>
      </Snackbar>
    </div>
  );
};
