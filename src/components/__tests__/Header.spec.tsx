import React from 'react';
import { createMemoryHistory } from 'history';
import { fireEvent, render, screen } from '@testing-library/react';
import { Router } from 'react-router-dom';
import { Header } from '../Header';
import { RouteLocations } from '../../config/Routes';
import { MemoryHistory } from 'history/createMemoryHistory';

describe('Header', () => {
  let history: MemoryHistory;
  const setUpHistory = () => {
    history = createMemoryHistory();
    history.push = jest.fn();
    const route = '/ticket-info';
    history.push(route);
  };

  const renderComponent = (showAddButton: boolean = true) => {
    setUpHistory();
    render(
      <Router history={history}>
        <Header showAddButton={showAddButton} />
      </Router>
    );
  };

  it('should not render add ticket button when show add button is false', () => {
    renderComponent(false);
    expect(screen.queryByRole('button')).toBeNull();
  });

  it('should render add ticket button when show add button is true', () => {
    renderComponent();
    expect(screen.getByRole('button')).not.toBeNull();
  });

  it('add new ticket button should call history push', () => {
    renderComponent();
    fireEvent.click(screen.getByRole('button'));
    expect(history.push).toHaveBeenCalled();
    expect(history.push).toHaveBeenCalledWith(RouteLocations.TICKET_FORM);
  });
});
