import React from 'react';
import { render } from '@testing-library/react';
import { NotFound } from '../NotFound';

describe('NotFound', () => {
  it('should render with not found content', () => {
    const dom = render(<NotFound />);
    expect(dom.container.querySelector('#notFound')).toBeInTheDocument();
    expect(dom.getByText('Page you were looking for can\'t be found')).toBeInTheDocument();
  });
});
