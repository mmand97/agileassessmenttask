import React from 'react';
import { render } from '@testing-library/react';
import { RecoilRoot } from 'recoil';
import { SuccessSnackbar } from '../SuccessSnackbar';
import { snackbarOpenState } from '../../store/SnackbarStore';

describe('SuccessSnackbar', () => {
  const initializeState = (isOpen: boolean) => ({ set }: any) => {
    set(snackbarOpenState, isOpen);
  };

  const renderComponent = (open: boolean = true) =>
    render(
      <RecoilRoot initializeState={initializeState(open)}>
        <SuccessSnackbar />
      </RecoilRoot>
    );

  it('should render snackbar when snackbar open is true', () => {
    const dom = renderComponent();
    expect(dom.container.querySelector('#snackbarSuccessMessage')).toBeInTheDocument();
  });

  it('should not render snackbar when snackbar open is false', () => {
    const dom = renderComponent(false);
    expect(dom.container.querySelector('#snackbarSuccessMessage')).not.toBeInTheDocument();
  });
});
