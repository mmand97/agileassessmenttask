import React from 'react';
import { Alert, Container } from '@mui/material';

export const NotFound = () => (
  <Container id="notFound" className="padding-top-100">
    <Alert id="notFoundAlert" severity="warning">Page you were looking for can't be found</Alert>
  </Container>
);
