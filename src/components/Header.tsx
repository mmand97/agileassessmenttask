import * as React from 'react';
import { Box, Toolbar, AppBar, Typography, Button, Link } from '@mui/material';
import { useHistory } from 'react-router-dom';
import { RouteLocations } from '../config/Routes';

export interface HeaderProps {
  showAddButton: boolean | undefined;
}

export const Header = ({ showAddButton }: HeaderProps) => {
  const { push } = useHistory();

  const handleOnClick = () => push(RouteLocations.TICKET_FORM);
  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static">
        <Toolbar>
          <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
            <Link href={`/app${RouteLocations.TICKETS}`} color="inherit" underline="none">
              Ticket Management App
            </Link>
          </Typography>
          { showAddButton && (
            <Button color="inherit" role="button" onClick={() => handleOnClick()}>Add new ticket</Button>
          )}
        </Toolbar>
      </AppBar>
    </Box>
  );
};
