import React from 'react';
import { createTheme, ThemeProvider } from '@mui/material';
import './theme/theme.scss';
import { RenderRoutes } from './config/Routes';
import { BrowserRouter } from 'react-router-dom';
import { RecoilRoot } from 'recoil';

function App() {
  const theme = createTheme({
    palette: {
      primary: {
        main: '#717070'
      }
    },
    components: {
      MuiGrid: {
        defaultProps: {
          xs: 12,
          sm: 12,
          md: 12,
          lg: 12,
          xl: 12
        }
      }
    }
  });

  return (
    <RecoilRoot>
      <BrowserRouter basename="/app">
        <ThemeProvider theme={theme}>
          <RenderRoutes />
        </ThemeProvider>
      </BrowserRouter>
    </RecoilRoot>
  );
}

export default App;
