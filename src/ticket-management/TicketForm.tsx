import React, { ReactElement, useState } from 'react';
import { Controller, useForm } from 'react-hook-form';
import { Button, Grid, TextField, Typography } from '@mui/material';
import { LocalizationProvider, DateTimePicker } from '@mui/lab';
import AdapterDateFns from '@mui/lab/AdapterDateFns';
import { useSetRecoilState } from 'recoil';
import { Ticket, ticketDataState } from '../store/TicketStore';
import { useHistory } from 'react-router-dom';
import { RouteLocations } from '../config/Routes';
import { v4 } from 'uuid';
import { snackbarOpenState } from '../store/SnackbarStore';
import { isValid } from 'date-fns';

export interface TicketFormInputs {
  description: string;
  dueDate: Date;
}

export const TicketForm = (): ReactElement => {
  const {
    register,
    handleSubmit,
    formState: { errors },
    control
  } = useForm<TicketFormInputs>();
  const { push } = useHistory();
  const setTickets = useSetRecoilState(ticketDataState);
  const setSnackbarOpen = useSetRecoilState(snackbarOpenState);

  const onSubmit = (data: TicketFormInputs) => {
    setTickets((oldTicketData: Ticket[]) => [
      ...oldTicketData,
      {
        key: v4(),
        description: data.description,
        dueDate: data.dueDate,
        creationDate: new Date()
      }
    ]);
    setSnackbarOpen(true);
    push(RouteLocations.TICKETS);
  };

  const getDateErrorLabel = (error: any) => {
    if (error?.type === 'validate') {
      return 'Date format is not valid';
    }
    if (error?.type === 'required') {
      return 'Due date is required';
    }
    return '';
  };

  return (
    <Grid container justifyContent="center" className="padding-100">
      <Grid item className="ticket-form-wrapper">
        <form onSubmit={handleSubmit(onSubmit)}>
          <Grid item>
            <Typography>
              Ticket info
            </Typography>
          </Grid>
          <Grid item className="padding-top-16">
            <TextField
              rows={4}
              id="description"
              multiline
              error={!!errors.description}
              helperText={errors?.description?.message}
              label="Description"
              {...register('description', { required: 'Description is required' })}
            />
          </Grid>
          <Grid item className="padding-top-16">
            <Controller
              control={control}
              name="dueDate"
              rules={{ required: 'Due date is required', validate: (value) => isValid(value) }}
              render={({
                field: { onChange, value },
                formState: { errors }
              }) => (
                <LocalizationProvider dateAdapter={AdapterDateFns}>
                  <DateTimePicker
                    value={value || null}
                    ampm={false}
                    inputFormat="dd/MM/yyyy HH:mm"
                    label="Ticket due date"
                    onChange={(date) => onChange(date)}
                    renderInput={(params) => (
                      <TextField
                        {...params}
                        id="dueDateInput"
                        error={!!errors.dueDate || params.error}
                        helperText={getDateErrorLabel(errors.dueDate)}
                      />
                    )}
                  />
                </LocalizationProvider>
              )}
            />
          </Grid>
          <Grid item container justifyContent="space-between" className="padding-top-16">
            <Button
              id="backButton"
              variant="contained"
              onClick={() => push(RouteLocations.TICKETS)}
            >
              Back
            </Button>
            <Button id="saveButton" variant="contained" type="submit" role="button">Save</Button>
          </Grid>
        </form>
      </Grid>
    </Grid>
  );
};
