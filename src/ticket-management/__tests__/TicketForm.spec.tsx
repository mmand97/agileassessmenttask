import React from 'react';
import { fireEvent, render, waitFor, screen } from '@testing-library/react';
import { TicketForm } from '../TicketForm';
import { RecoilRoot } from 'recoil';
import { createMemoryHistory } from 'history';
import { Router } from 'react-router-dom';
import { RecoilObserver } from '../../utils/TestUtil';
import { ticketDataState } from '../../store/TicketStore';
import { snackbarOpenState } from '../../store/SnackbarStore';
import { overDueTicketsMockData } from '../../data/TicketData';
import userEvent from '@testing-library/user-event';
import MockDate from 'mockdate';

jest.mock('uuid', () => ({ v4: () => '00000000-0000-0000-0000-000000000000' }));

describe('TicketForm', () => {
  const setTickets = jest.fn();
  const setSnackbarOpen = jest.fn();

  const renderComponent = () => {
    const history = createMemoryHistory();
    const route = '/ticket-info';
    history.push(route);
    return render(
      <RecoilRoot>
        <RecoilObserver node={ticketDataState} onSubmit={setTickets} />
        <RecoilObserver node={snackbarOpenState} onSubmit={setSnackbarOpen} />
        <Router history={history}>
          <TicketForm />
        </Router>
      </RecoilRoot>
    );
  };

  it('should render all form elements', () => {
    const dom = renderComponent();
    expect(dom.container.querySelector('#description')).toBeInTheDocument();
    expect(dom.container.querySelector('#dueDateInput')).toBeInTheDocument();
    expect(dom.container.querySelector('#backButton')).toBeInTheDocument();
    expect(dom.container.querySelector('#saveButton')).toBeInTheDocument();
  });

  describe('should display required error when', () => {
    it('description value is not filled', async () => {
      const dom = renderComponent();
      const descriptionInput = dom.container.querySelector('#description');
      const submitButton = dom.container.querySelector('#saveButton') as Element;

      expect(descriptionInput).not.toHaveValue();

      fireEvent.submit(submitButton);
      expect(await screen.findByText('Description is required')).not.toBeNull();
    });
    it('due date value is not filled', async () => {
      const dom = renderComponent();
      const submitButton = dom.container.querySelector('#saveButton') as Element;
      const dueDateInput = dom.container.querySelector('#dueDateInput') as Element;

      expect(dueDateInput).not.toHaveValue();

      userEvent.click(submitButton);
      expect(await screen.findByText('Due date is required')).not.toBeNull();
    });
  });

  it('should save form data and not display error when values are valid', async () => {
    const dom = renderComponent();
    const submitButton = dom.container.querySelector('#saveButton') as Element;
    const descriptionInput = dom.container.querySelector('#description') as Element;
    const dueDateInput = dom.container.querySelector('#dueDateInput') as Element;
    MockDate.set('01/01/2020');

    userEvent.type(descriptionInput, 'test description');
    userEvent.type(dueDateInput, '01/01/2020 00:00');
    userEvent.click(submitButton);

    await waitFor(() => {
      expect(screen.queryByText('Description is required')).toBeNull();
      expect(screen.queryByText('Due date is required')).toBeNull();
      expect(setSnackbarOpen).toHaveBeenCalledTimes(2);
      expect(setTickets).toHaveBeenCalledTimes(2);
      expect(setSnackbarOpen).toHaveBeenCalledWith(true);
      expect(setTickets).toHaveBeenCalledWith(overDueTicketsMockData);
    });

    MockDate.reset();
  });
});
