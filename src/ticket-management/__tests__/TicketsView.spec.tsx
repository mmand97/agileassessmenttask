import React from 'react';
import { fireEvent, render, screen, waitFor } from '@testing-library/react';
import { RecoilRoot } from 'recoil';
import { TicketsView } from '../TicketsView';
import { Router } from 'react-router-dom';
import { createMemoryHistory } from 'history';
import { Ticket, ticketDataState } from '../../store/TicketStore';
import { overDueTicketsMockData, ticketsMockData } from '../../data/TicketData';
import { RecoilObserver } from '../../utils/TestUtil';
import { snackbarOpenState } from '../../store/SnackbarStore';
import userEvent from '@testing-library/user-event';

describe('TicketsView', () => {
  const initializeState = (mockData: Ticket[]) => ({ set }: any) => {
    set(ticketDataState, mockData);
  };

  const setTickets = jest.fn();
  const setSnackbarOpen = jest.fn();

  const renderComponent = (initialState: Ticket[] = []) => {
    const history = createMemoryHistory();
    const route = '/ticket-info';
    history.push(route);
    return render(
      <RecoilRoot initializeState={initializeState(initialState)}>
        <RecoilObserver node={ticketDataState} onSubmit={setTickets} />
        <RecoilObserver node={snackbarOpenState} onSubmit={setSnackbarOpen} />
        <Router history={history}>
          <TicketsView />
        </Router>
      </RecoilRoot>
    );
  };

  describe('should render', () => {
    it('table alert instead of table', () => {
      const dom = renderComponent();
      expect(dom.container.querySelector('#table-alert')).toBeInTheDocument();
      expect(dom.container.querySelector('#ticket-table')).not.toBeInTheDocument();
    });

    it('table instead of table alert', () => {
      const dom = renderComponent(ticketsMockData);
      expect(dom.container.querySelector('#ticket-table')).toBeInTheDocument();
      expect(dom.container.getElementsByClassName('ticket-over-due')).toHaveLength(0);
      expect(dom.container.querySelector('#table-alert')).not.toBeInTheDocument();
    });
  });

  it('icon button should remove ticket from the list of tickets and show success', async () => {
    const dom = renderComponent(ticketsMockData);
    const completeTicketButton = dom.container.querySelector('#ticket-complete-button') as Element;

    expect(dom.container.querySelector('#ticket-table')).toBeInTheDocument();

    userEvent.click(completeTicketButton);

    await waitFor(() => {
      expect(setTickets).toHaveBeenCalled();
      expect(setSnackbarOpen).toHaveBeenCalled();
      expect(setTickets).toHaveBeenCalledWith([]);
      expect(setSnackbarOpen).toHaveBeenCalledWith(true);
      expect(dom.container.querySelector('#table-alert')).toBeInTheDocument();
    });
  });

  it('should render overdue ticket with ticket-over-due class', () => {
    const dom = renderComponent(overDueTicketsMockData);
    expect(dom.container.querySelector('#table-alert')).not.toBeInTheDocument();
    expect(dom.container.querySelector('#ticket-table')).toBeInTheDocument();
    expect(dom.container.getElementsByClassName('ticket-over-due')[0]).toBeInTheDocument();
  });
});
