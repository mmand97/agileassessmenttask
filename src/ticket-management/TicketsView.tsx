import React, { ReactElement } from 'react';
import {
  Alert,
  Grid,
  IconButton,
  Paper, Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead, TablePagination,
  TableRow,
  Tooltip
} from '@mui/material';
import { dateToString, getTime } from '../utils/DateUtil';
import { useRecoilState, useSetRecoilState } from 'recoil';
import { Ticket, ticketDataState } from '../store/TicketStore';
import { differenceInMilliseconds } from 'date-fns';
import { CheckCircle } from '@mui/icons-material';
import { snackbarOpenState } from '../store/SnackbarStore';

const ONE_HOUR = 60 * 60 * 1000;

export const TicketsView = (): ReactElement => {
  const [page, setPage] = React.useState(0);
  const setSnackbarOpen = useSetRecoilState(snackbarOpenState);

  const handleChangePage = (event: unknown, newPage: number) => {
    setPage(newPage);
  };
  const [tickets, setTickets] = useRecoilState<Ticket[]>(ticketDataState);

  const ticketOverDue = (ticketDueDate: Date): boolean =>
    differenceInMilliseconds(new Date(ticketDueDate), new Date()) < ONE_HOUR;

  const setTicketComplete = (ticketKey: string) => {
    setTickets((ticketData: Ticket[]) => ticketData.filter((ticket) => ticket.key !== ticketKey));
    setSnackbarOpen(true);
  };

  const emptyRows = page > 0 ? Math.max(0, (1 + page) * 5 - tickets.length) : 0;

  const getSortedAndFilterTickets = (): Ticket[] =>
    tickets?.slice().sort((ticket1, ticket2) => getTime(ticket2.dueDate) - getTime(ticket1.dueDate))
      .slice(page * 5, page * 5 + 5);

  if (!tickets.length) {
    return (
      <Grid id="table-alert" container justifyContent="center" className="ticket-table-wrapper">
        <Grid item>
          <Alert severity="info" role="alert">There is currently no table data to show. Please add a new ticket.</Alert>
        </Grid>
      </Grid>
    );
  }

  return (
    <Grid id="ticket-table" container justifyContent="center" className="ticket-table-wrapper">
      <Grid item>
        <TableContainer component={Paper}>
          <Table sx={{ minWidth: 250 }} aria-label="ticket table" role="table">
            <TableHead>
              <TableRow>
                <TableCell>Ticket description</TableCell>
                <TableCell align="right">Creation date</TableCell>
                <TableCell align="right">Due date</TableCell>
                <TableCell align="right" />
              </TableRow>
            </TableHead>
            <TableBody>
              {getSortedAndFilterTickets().map((row: Ticket) => (
                <TableRow
                  key={row.key}
                  sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                  className={ticketOverDue(row.dueDate) ? 'ticket-over-due' : ''}
                >
                  <TableCell component="th" scope="row">
                    {row.description}
                  </TableCell>
                  <TableCell align="right">{dateToString(row.creationDate)}</TableCell>
                  <TableCell align="right">{dateToString(row.dueDate)}</TableCell>
                  <TableCell align="center">
                    <Tooltip title="Complete ticket" arrow>
                      <IconButton onClick={() => setTicketComplete(row.key)} id="ticket-complete-button">
                        <CheckCircle className="complete-ticket-icon" />
                      </IconButton>
                    </Tooltip>
                  </TableCell>
                </TableRow>
              ))}
              {emptyRows > 0 && (
                <TableRow
                  style={{
                    height: 72.7 * emptyRows
                  }}
                >
                  <TableCell colSpan={6} />
                </TableRow>
              )}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          component="div"
          rowsPerPageOptions={[]}
          count={tickets.length}
          rowsPerPage={5}
          page={page}
          onPageChange={handleChangePage}
        />
      </Grid>
    </Grid>
  );
};
