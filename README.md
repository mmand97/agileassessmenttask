## Setup:
ESlint: File -> Settings -> Languages & Frameworks -> JavaScript -> Code Quality Tools -> ESLint -> Select Manual ESLint configuration -> Choose ESLint package and config file location.
## Dev:
To start the dev server run `yarn start`.
Navigate to [http://localhost:3000](http://localhost:3000) to view it in the browser.
## Test:
Run `yarn test` to test the application.
## Build
To create a optimized production build run `yarn build`
## Docker development
Linux/Mac OS:
1. In terminal run `sudo docker build -t agile-app:dev .` in the root of the project
2. To start the container run `sudo docker run -it --rm -v ${PWD}:/app -v /app/node_modules -p 3001:3000 -e CHOKIDAR_USEPOLLING=true agile-app:dev`
3. After the container starts it will be available at [http://localhost:3001](http://localhost:3001)
## Production
App is deployed through GitLab CI (See gitlab-ci.yml for more info) to Heroku and accessible at [https://agile-test-job-production.herokuapp.com/app/tickets](https://agile-test-job-production.herokuapp.com/app/tickets)
